# html-ext

Extract structured data and useful data from html text

## Usage

Run test suite

    $ clojure -M:test
    
Build a deployable jar of this library:

    $ clojure -M:jar

Install it locally:

    $ clojure -M:install

Deploy it to Clojars -- needs `CLOJARS_USERNAME` and `CLOJARS_PASSWORD` environment variables:

    $ clojure -M:deploy

## License

Copyright © 2023 Matteo Redaelli

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
